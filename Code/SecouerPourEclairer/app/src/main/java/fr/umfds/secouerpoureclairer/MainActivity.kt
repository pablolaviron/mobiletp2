package fr.umfds.secouerpoureclairer

import android.content.Context
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.hardware.camera2.CameraManager
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), SensorEventListener {

    private lateinit var infoTv: TextView
    private var accelerometer: Sensor? = null
    private lateinit var sensorManager: SensorManager
    private lateinit var cameraManager: CameraManager
    private lateinit var camera: String
    private var flashOn: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        infoTv = findViewById(R.id.info_tv)

        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        camera = cameraManager.cameraIdList[0]
    }

    override fun onResume() {
        super.onResume()
        accelerometer?.also { acc ->
            sensorManager.registerListener(this, acc, SensorManager.SENSOR_DELAY_NORMAL);
        }
        cameraManager.setTorchMode(camera, flashOn)
    }

    override fun onPause() {
        super.onPause()
        sensorManager.unregisterListener(this)
        cameraManager.setTorchMode(camera, false)
    }

    override fun onSensorChanged(event: SensorEvent?) {
        event?.let {
            val sensorValue: Float = it.values[0]
            if (sensorValue > 20) {
                if (flashOn) {
                    infoTv.text = "Éteint !"
                    flashOn = false
                } else {
                    infoTv.text = "Allumé !"
                    flashOn = true
                }
            }

            cameraManager.setTorchMode(camera, flashOn)
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {

    }

}
