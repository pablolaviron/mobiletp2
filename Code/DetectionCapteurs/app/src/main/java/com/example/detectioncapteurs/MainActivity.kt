package com.example.detectioncapteurs

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var proximityBt: Button
    private lateinit var heartRateBt: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        proximityBt = findViewById(R.id.proximity_bt)
        heartRateBt = findViewById(R.id.heart_rate_bt)

        var sensorManager: SensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager

        proximityBt.setOnClickListener {
            if (sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null) {
                Toast.makeText(this, "You have Proximity Sensor, great!", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "You don't have Proximity Sensor, sad...", Toast.LENGTH_LONG).show()
            }
        }

        heartRateBt.setOnClickListener {
            if (sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE) != null) {
                Toast.makeText(this, "You have Heart Rate Sensor, great!", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "You don't have Heart Rate Sensor, sad...", Toast.LENGTH_LONG).show()
            }
        }
    }

}