package com.example.listecapteurs

import android.hardware.Sensor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class SensorAdapter(private var sensors: List<Sensor>) : RecyclerView.Adapter<SensorAdapter.SensorViewHolder>() {

    fun updateData(newSensors: List<Sensor>) {
        sensors = newSensors
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SensorViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_sensor, parent, false)
        return SensorViewHolder(view)
    }

    override fun onBindViewHolder(holder: SensorViewHolder, position: Int) {
        val schedule = sensors[position]
        holder.bind(schedule)
    }

    override fun getItemCount(): Int = sensors.size

    class SensorViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val sensorName: TextView = itemView.findViewById(R.id.sensor_name)

        fun bind(sensor: Sensor) {
            sensorName.text = sensor.name
        }
    }

}