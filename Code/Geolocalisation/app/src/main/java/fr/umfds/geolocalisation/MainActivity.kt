package fr.umfds.geolocalisation

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlin.math.round

class MainActivity : AppCompatActivity() {

    private lateinit var locationTv: TextView
    private lateinit var locationManager: LocationManager
    private lateinit var openMapBt: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        locationTv = findViewById(R.id.location_tv)
        openMapBt = findViewById(R.id.open_map_bt)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        locationManager.getCurrentLocation(LocationManager.GPS_PROVIDER, null, mainExecutor) {
            val location = it
            locationTv.text = "Latitude: %.2f\nLongitude: %.2f".format(location.latitude, location.longitude)
            openMapBt.setOnClickListener {
                val uri: Uri = Uri.parse("${location.latitude},${location.longitude}")
                val label = Uri.encode("Ma position")
                val uriString = "geo:0,0?q=$uri($label)"
                val finalUri = Uri.parse(uriString)
                val intent = Intent(Intent.ACTION_VIEW, finalUri)
                startActivity(intent)
            }
        }
        locationTv.text = "Récupération de la position..."
    }

}
